# Backups
RMT Local Infrastructure Backups

# GPG
The backups are encryped with GPG. You can locate the encryption script in scripts directory in this repository.
There is also an encryption ci job located in gitlab-ci/encrypt.yml. If you want to add or remove gpg public keys you can locate them in ght gpg_keys directory.
After you add or remove the key you have to modify the encrypt.sh script in the scripts directory to reflect the change. Just add or remove the recipient line.

scripts/encrypt.yml
```bash
  gpg --batch --yes --always-trust --encrypt \
  --recipient nate@redmoose.tech \
  --recipient example@redmoose.tech \ 
  --output "$f".gpg "$f"
  ```
- see example@p3f.llc


# Updates
This is pretty self explanatory, to add to the updates you need to add a ansible playbook that will backup a specific service. You add that playbook to the services directory in this repository and add a line in backup.yml to import that playbook.
You also need to add a job in backup.yml in gitlab-ci directory in repository.

backup.yml

```yaml
# add an import line below
---
- import_playbook: services/core-router.yml
- import_playbook: services/staff-routers.yml
- import_playbook: services/youtrack.yml
- import_playbook: services/gitlab.yml
- import_playbook: services/nextcloud.yml
- import_playbook: services/awx.yml
- import_playbook: services/example.yml # <<<--- Example
```


gitlab-ci/backup.yml
```yaml
# add an import line below
manual:backup:awx:
  script:
    - "mv ansible.cfg hosts /etc/ansible/"
    - ansible-playbook services/example.yml # <<<--- Example 
  extends: [.backup-manual]

# Scheduled Backup Jobs
scheduled:backup:awx:
  script:
    - "mv ansible.cfg hosts /etc/ansible/"
    - ansible-playbook services/example.yml # <<<--- Example 
  extends: [.backup-scheduled]
```


also in the gitlab-ci/report.yml file you want to add the new job as a dependency for reporting.
```yaml
.report:
  stage: report
  dependencies:
    - scheduled:backup:vulcansarsenal
    - scheduled:backup:staff-routers
    - scheduled:backup:youtrack
    - scheduled:backup:gitlab
    - scheduled:backup:nextcloud
    - scheduled:backup:awx
    - scheduled:backup:example # <<<--- Example 
    - scheduled:encrypt
    - scheduled:retention_policy
    - scheduled:sync
  extends: [.ssh, .ansible, .schedules]
```

# Gotcha's
- The STACK variable in the ansible scripts is set to a gitlab-ci variable named `$CI_COMMIT_BRANCH`. This variable is simply the name of the working branch.
  You can read more about gitlab predefined variables [here](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) </br>
- Another solid mention is that this is using an ansible runner for most of the jobs. The tag for that runner is uatu-ansible

# References
https://docs.gitlab.com/ee/ci/yaml/
