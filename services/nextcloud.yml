---
- name: Backup Nextcloud
  hosts: nextcloud.rmt
  gather_facts: yes
  become: yes
  vars:
    service: "nextcloud"
    today: "{{ ansible_date_time.date }}"
    backup_dest: "/backups/infrastructure/daily/{{ today }}/{{ service }}"
    nextcloud_data_dir: "/var/lib/docker/volumes/nextcloud/_data"
    nextcloud_latest: "/backups/infrastructure/daily/nextcloud-latest"
  tasks:
    - name: Create Backup Directory
      file:
        path: "{{ backup_dest }}"
        state: directory
        recurse: yes
      delegate_to: localhost
      connection: local

    - block:
        - name: Nextcloud Maintenance Mode Disabled
          shell: docker exec -t nextcloud sudo -u www-data php -dmemory_limit=-1 /var/www/html/occ maintenance:mode --on

        - name: Create Postgres Backup
          shell: docker exec -t postgres pg_dumpall -U nextcloud > /tmp/nextcloud.postgres.dmp

        - name: Pull Nextcloud Postgres
          synchronize:
            src: "/tmp/nextcloud.postgres.dmp"
            dest: "{{ backup_dest }}/nextcloud.postgres.dmp"
            mode: pull
            archive: no
            rsync_opts:
              - "--remove-source-files"

        - name: Pull Nextcloud Data
          synchronize:
            src: "{{ nextcloud_data_dir }}/"
            dest: "{{ backup_dest }}/data/"
            archive: no
            private_key: ~/.ssh/ansible_ecdsa
            link_dest: "{{ nextcloud_latest }}/"
            set_remote_user: no
            mode: pull
            rsync_opts:
              - "-rlptD"
              - "--no-i-r"
              - "--delete"

        - name: Add Hardlink to latest Nextcloud Data
          shell:
            cmd: |
              rm -rf {{ nextcloud_latest }}/
              cp -al {{ backup_dest }}/data/ {{ nextcloud_latest }}/
            warn: false
          delegate_to: localhost
          connection: local
      always:
        - name: Nextcloud Maintenance Mode Enabled
          shell: docker exec -t nextcloud sudo -u www-data php -dmemory_limit=-1 /var/www/html/occ maintenance:mode --off
